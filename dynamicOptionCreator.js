﻿$(document).ready(function () {
    var name;
    var form;

    //Open add cat div
    $("button[type='button']").not("#catCancel,#catSubmit").on("click", function () {
        name = this.name;
        if ($("#notification").dialog("isOpen") == false) {
            $("#AddCatTitle").text("Add New " + this.name);
            $("#AddCatInput").attr('placeholder', "Enter " + this.name);
            $("#notification").dialog("open");
        }
    });
    // Close add cat div
    $("#catCancel").on("click", function () {
        $("#notification").dialog("close");
        $('#addcat span[id^="reg-error"]').remove();
    });

    //On submit
    $("#catSubmit").on("click", function () { //string nameField, string input
        
        $.ajax({
            url: linkToValidateController.Validate,
            type: "GET",
            data: { input: $("#Name").val(), nameField: name, comp: $('#Company').val(), sup:$('#SupplierName').val()},
            success: function (data) {
                if (data) {
                    $("#addcat").append("<span id='reg-error' class='alert post warning'>That " + name + " Exists</span>");
                    $("#addcomp").append("<span id='reg-error' class='alert post warning'>That " + name + " Exists</span>");
                    $("#addsup").append("<span id='reg-error' class='alert post warning'>That " + name + " Exists</span>");
                } else {

                        //Adding a new manufacturer
                    if (name == "Manufacturer") {
                        form = $("#addcat");
                        $.ajax({
                            url: linkToValidateController.CreateMan,
                            type: "POST",
                            async: false,
                            data: form.serialize(),
                            success: function(data) {
                                $("#notification").dialog("close");
                                $('#addcat span[id^="reg-error"]').remove();
                                $('#ManufacturerID').append('<option value=' + data.ManufacturerID + '>' + data.Name + '</option>'); //name of the record retrieved
                                $('#ManufacturerID').trigger("chosen:updated");
                                $("body").append("<span id='reg-error' class='alert post success'>New " + name + " Created</span>"); //name of the select
                            }
                        });
                    }

                        //Adding a lease company
                    else if (name == "LeaseCompanyID") {
                        form = $("#addcomp");
                        $.ajax({
                            url: linkToValidateController.CreateLes,
                            type: "POST",
                            async: false,
                            data: form.serialize(),
                            success: function (data) {
                                $("#notification").dialog("close");
                                $('#addcomp span[id^="reg-error"]').remove();
                                $('#LeaseCompanyID').append('<option value=' + data.LeaseCompanyID + '>' + data.Company + '</option>'); //name of the record retrieved
                                $('#LeaseCompanyID').trigger("chosen:updated");
                                $("body").append("<span id='reg-error' class='alert post success'>New " + name + " Created</span>"); //name of the select
                            }
                        });
                    }

                        //Adding a support company
                    else if (name == "SupplierName") {
                        form = $("#addsup");
                        $.ajax({
                            url: linkToValidateController.CreateSup,
                            type: "POST",
                            async: false,
                            data: form.serialize(),
                            success: function (data) {
                                console.log(data);
                                $("#notification").dialog("close");
                                $('#addsup span[id^="reg-error"]').remove();
                                $('#SupplierID').append('<option value=' + data.SupplierID + '>' + data.SupplierName + '</option>'); //name of the record retrieved
                                $('#SupplierID').trigger("chosen:updated");
                                $("body").append("<span id='reg-error' class='alert post success'>New " + name + " Created</span>"); //name of the select
                            }
                        });
                    }
                }
            },
        });

    });
});