﻿/*
Dynamic form validation on the clientside for form fields
*/

$(document).ready(function () {

    var nullable = ["Description", "ContractLength"]; // give this array elements of the input name of nullables in a form, i.e description can be stored as null on the database

    // Makes sure only numbers can be typed into number fieldsnu
    $(document).ready(function () {
        $("input[type='number'").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                if (!(e.preventDefault())) {
                    e.returnValue = false;
                } else {
                    e.preventDefault();
                }
            }
        });
    });


    //Mmove this into init or core after testing 
    $(".select-jquery").each(function () {
        var name = this.id;
        // Remove last 2 characters which are ID and add a placeholder to each select box
        $(this).prepend("<option value='0' selected disabled>Select a " + name.substring(0, name.length - 2) + "</option>");    
    });

    //Initializer date pickers
    $(".datepicker").datepicker();

    //Initalize drop down menusno
    $('select').chosen();

    // ^^^^^^^^^ALL THIS^^^^^^^^

    var inputName;
    var divName;
    var classSuc;

    $("input,textarea,a").click(function() { // When an input is clicked set inputName and divName
        inputName = this.id;
        divName = ($(this).parent('div').attr('id'));
        if (divName.indexOf("chosen") > -1) {
            divName = ($(this).parent().parent('div').attr('id')); // Gets grandparent div instead

        }
        if (nullable.indexOf(inputName) < 0) {  //Checks the nullable array to see if the input selected is allowed to have null as its value
            $('#' + inputName).focusout(function() {

                //Ajax Call for checking unique attributes testing Registration field only
                var uniqueIdentifiers = ['Registration', 'ContractNumber', 'SN', 'AssetTag'];
                if (uniqueIdentifiers.indexOf(inputName) > -1) {
                    var value = $("#" + inputName).val();
                    var model = $("#title").attr('name'); // Useful for models that have the same unique identifier
                    $.ajax({
                        url: "CheckIfExists",
                        type: "GET",
                        data: { name: value, div: inputName, model: model },
                        async: false, //temp fix for now
                        success: function(data) {
                            if (data) {
                                // Reg exists show prompt
                                $("#" + inputName).val('');
                                validChecker();
                                if (!($('#reg-error').length)) {
                                    $("body").append("<span id='reg-error' class='alert post warning'>" + inputName + " already exists </span>");
                                }
                            } else {
                                $('span[id^="reg-error"]').remove();
                                validChecker();
                            }
                        },
                    });
                } else {
                    if (inputName == "ContractStart" || inputName == "ContractExpire") {  // The delay from the timeout is needed so it validates correct
                        setTimeout(validChecker, 200);
                    } else {
                        validChecker();
                    }

                }
            });
        }


        function validChecker() {
            /*
                Checks inputs and adds error css class or success css class
            */

            // If there is no text remove valid class and add error
            if ((!$('#' + inputName).val())) {
                if ($('#' + divName).hasClass("success")) {
                    $('#' + divName).removeClass("success");
                }
                if (!$('#' + divName).hasClass("warning")) {
                    $('#' + divName).addClass("warning");
                }
            }
                // If there is text remove error class and add valid
            else {
                if ($('#' + divName).hasClass("warning")) {
                    $('#' + divName).removeClass("warning");
                }
                if (!$('#' + divName).hasClass("success")) {
                    $('#' + divName).addClass("success");
                }
            }

            // This block checks for validation on the contract start and contract expire dates
            if ($("#title").attr('name') == "Support" || $("#title").attr('name') == "Lease") {
                if (inputName == "ContractStart" || inputName == "ContractExpire")
                {
                    if (checkDate($('#' + inputName).val()) == false)
                    {
                        if ($('#' + divName).hasClass("success")) {
                            $('#' + divName).removeClass("success");
                        }
                        if (!$('#' + divName).hasClass("warning")) {
                            $('#' + divName).addClass("warning");
                        }
                    }
                }

            }

            // If contract length is invalid then error the Contract expire Div
            if ($("#ContractLength").val() < 0) {
                if ($('#CoeDiv').hasClass("success")) {
                    $('#CoeDiv').removeClass("success");
                }
                if (!$('#CoeDiv').hasClass("warning")) {
                    $('#CoeDiv').addClass("warning");
                }

            }
        }
        // Do validation check for select drop down boxes, if it is null give a warning
        $('select').on('chosen:hiding_dropdown', function () {
            if ($(this).val() === null) {
                $('#' + divName).addClass("warning");
            } else {
                $('#' + divName).removeClass("warning");
                $('#' + divName).addClass("success");
            }
           
        });

        // Returns false if the date entered is incorrect
        function checkDate(str) {
            var matches = str.match(/(\d{1,2})[- \/](\d{1,2})[- \/](\d{4})/);
            if (!matches) return false;

            // parse each piece and see if it makes a valid date object
            var month = parseInt(matches[1], 10);
            var day = parseInt(matches[2], 10);
            var year = parseInt(matches[3], 10);
            var date = new Date(year, month - 1, day);
            if (!date || !date.getTime()) return false;

            // make sure we have no funny rollovers that the date object sometimes accepts
            // month > 12, day > what's allowed for the month
            if (date.getMonth() + 1 != month ||
                date.getFullYear() != year ||
                date.getDate() != day) {
                return false;
            }
            return (date);
        }

        // Given 2 date objects, will give the difference between the 2 in months
        // Can be changed to return days or years if needed
        function calcDate(date1, date2) {
            var diff = Math.floor(date1.getTime() - date2.getTime());
            var day = 1000 * 60 * 60 * 24;

            var days = Math.floor(diff / day);
            var months = Math.floor(days / 31);
            //var years = Math.floor(months / 12);

            return months*-1;
        }

        // Set contract Length Box
        $('#ContractStart,#ContractExpire').change(function () {
            //Set Contract Length
            var startparts = $("#ContractStart").val().split('/');
            var endparts = $("#ContractExpire").val().split('/');
            var startdate = new Date(startparts[2], startparts[0] - 1, startparts[1]);
            var enddate = new Date(endparts[2], endparts[0] - 1, endparts[1]);

            //When dates are chosen calculate the months
            if ($("#ContractStart").val() != '' && $("#ContractExpire").val() != '') {
                $("#ContractLength").val((calcDate(startdate, enddate))); // Update contract length
            } else {
                $("#ContractLength").val(0);   // Set contract length to 0
            }
            validChecker();

        });
    });

    /*
    The following function is to check all the inputs to ensure that none are errored
    Currently its attached to the testing button. My design thoughts for this
    Link the submit button to this function, if this function returns true the form is valid.
    If this function finds an error it will create an alert box (for now)
    */
    $("#testing").click(function() {
        alert(checkForErrors());
    });

    function checkForErrors() {
        /*
        Returns the number of errors on the page
        */
        var errors = 0;
        var elements = document.querySelectorAll('.panel input,.panel select,.panel textarea');
        for (var i = 0; i < elements.length; i++) {
            if ($(elements[i]).is(":visible") || $(elements[i]).is("select")) { // check for selects they are invisible
                divName = ($('#' + (elements[i].id)).parent('div').attr('id'));
                classSuc = String($('#' + divName).attr('class')); // This line is suspect 
                inputName = elements[i].id;
                //alert(inputName);
                if (nullable.indexOf(inputName) < 0) { //Will only check inputs where it can't be null

                    //                                      (\/) 
                    //I'll leave this in for testing Paul  (0.0)
                    //                                      (><).   -Bugs Bunny.

                    //if (divName && classSuc.indexOf("success") >= 0) { //                     
                    //    alert("This is a success " + divName);
                    //} else 

                    if ((divName && classSuc.indexOf("success") == -1)) {
                        errors++;
                    }
                }
            }
        }
        return errors;
    }

});


